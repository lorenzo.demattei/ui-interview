# UI Interview

The scope of this exercise is to create a tiny small web application using Angular.

To do this, please create a new application with the Angular CLI.

```
ng new daitomic-interview
```

### Description

The application is very simple: you’ll be provided with some blocks of text 
and a list of definitions.

For example, given a block of text, you can have a list of definitions with the following interface:

```
interface Definition {
   offset: {
     startChar: number;
     endChar: number;
   }
}
```

A definition represents a string of text within the block of text. The 
offset object provides you with the definition's start and end indexes 
within the text.

Given an HTML node:

```angular2html
<div>
  This is the node’s text
</div>
```

Given the following definition:

```typescript
{
  offset: {
    startChar: 6,
    endChar: 7
  }
}
```

The directive will output the following result:

```angular2html
<div>
  This <definition>is</definition> the node’s text
</div>
```

Do approach this using an Angular directive placed on the parent element of the text.
For example:

```angular2html
<div definitionsHighlighter>{{ text }}</div>
```

Please do style the definition as you wish - as long as it is clearly visible.

Here is a rendered example:

![Example of definition](./definition_example.png)

You can find [here](data.json) a few sample data. 